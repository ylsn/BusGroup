$(document).ready(function () {
    $("li.select").click(function () {
        $("input[name='adminPower']").val($(this).text());
        $("input[name='adminPower']").data("power",$(this).val());
    })

    $("button.btn.btn-info.btn-fill.btn-wd").click(function () {
        var staff={};
        staff.adminId=$("input[name='adminId']").val();
        if(staff.adminId==""){
            alert("请输入账号");
            return false;
        }
        if(eval(staff.adminId.length)<8){
            alert("账号不低于8位！");
            return false;
        }

        staff.adminPassword=$("input[name='adminPassword']").val();
        if(staff.adminPassword==""){
            alert("请输入密码");
            return false;
        }
        if(eval(staff.adminPassword.length)<=8){
            alert("请使用更加复杂的密码!");
            return false;
        }
        staff.adminOwner=$("input[name='adminOwner']").val();
        if(staff.adminOwner==""){
            alert("请输入所属公司");
            return false;
        }
        staff.adminPower=$("input[name='adminPower']").data("power");
        if(staff.adminPower==""){
            alert("请选择职位信息");
            return false;
        }
        $.ajax({
            type: 'POST',
            url: 'addAdmin',
            contentType: "application/json;charset=utf-8",
            DataType: "json",
            data: JSON.stringify(staff),
            error: function () {
                alert("加载失败，请刷新重试！");
            },
            success: function (res) {
                confirm("添加成功,是否继续？");
                if(confirm)
                    window.location.reload();
            }
        })
    })
})